(ns stash.transit-test
  (:require #?(:clj [clojure.test :refer :all]
               :cljs [cljs.test :refer-macros [deftest is]])
            [stash.transit :as transit]
            [time-literals.read-write]
            [time-literals.data-readers])
  #?(:clj (:import [java.time LocalDate])))

(deftest ^:unit write-str-test
  (is (= "[\"^ \"]"
         (transit/write-str {})))
  (is (= "[\"^ \",\"~:a\",1]"
         (transit/write-str {:a 1}))))

(deftest ^:unit read-test
  (is (= {}
         (transit/read "[\"^ \"]")))
  (is (= {:a 1}
         (transit/read "[\"^ \",\"~:a\",1]"))))

#?(:clj
   (do
     (deftest print-time-literal
       (time-literals.read-write/print-time-literals-clj!)
       (is (= "#time/date \"2020-05-20\""
              (pr-str (LocalDate/parse "2020-05-20"))))
       (is (= #time/date "2020-05-20"
              (LocalDate/parse "2020-05-20"))))

     (deftest roundtrip-date-through-transit
       (is (= #time/date "2020-05-20"
              (-> (LocalDate/parse "2020-05-20")
                  transit/write-str
                  transit/read))))))