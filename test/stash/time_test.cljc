(ns stash.time-test
  (:require #?(:clj  [clojure.test :refer :all]
               :cljs [cljs.test :refer-macros [deftest is]])
            [stash.time :as time]
            [tick.core :as t]))

(deftest time-ago-test
  (is (= "5 seconds ago" (time/time-ago (t/<< (t/now) (t/new-duration 5 :seconds)))))
  (is (= "5 minutes ago" (time/time-ago (t/<< (t/now) (t/new-duration 5 :minutes)))))
  (is (= "5 hours ago" (time/time-ago (t/<< (t/now) (t/new-duration 5 :hours)))))
  (is (= "5 days ago" (time/time-ago (t/<< (t/now) (t/new-duration 5 :days)))))
  (is (= "2 weeks ago" (time/time-ago (t/<< (t/now) (t/new-period 2 :weeks)))))
  (is (= "2 months ago" (time/time-ago (t/<< (t/now) (t/new-period 10 :weeks)))))
  (is (= "1 year ago" (time/time-ago (t/<< (t/now) (t/new-period 60 :weeks)))))

  (is (nil? (time/time-ago nil))))