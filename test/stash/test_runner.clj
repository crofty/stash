(ns stash.test-runner
  (:require [eftest.runner :as ef]))

(def test-parallelism 4)

(defn run-tests []
  (as-> (ef/find-tests ["test"]) test-vars
        (ef/run-tests test-vars
                      {:multithread true
                       :thread-count test-parallelism})))

(defn -main []
  (let [{:keys [error fail]} (run-tests)]
    (when-not (and (zero? error) (zero? fail))
      (System/exit (Math/min 255 (+ error fail))))))