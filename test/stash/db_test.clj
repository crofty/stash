(ns stash.db-test
  (:require [clojure.test :refer :all]
            [stash.db.url :as db.url]
            [honeysql.core :as sql]
            [stash.db :as db]))

(defn with-system [f]
  (let [database-url (System/getenv "DATABASE_URL")]
    (assert database-url)
    (let [db (->
              (db.url/parse database-url)
              (db.url/->jdbc-url)
              db/from-url)]
      (f {:db db})
      (.close db))))

(deftest select-test
  (with-system
   (fn [{:keys [db]}]

     (testing "selecting with strings"
       (is (= 1 (-> (db/find-first db "select 1 as one")
                    :one)))
       (testing "selecting JSON"
         (is (= {:name "stash" :id 1}
                (-> (db/find-first db "select json_build_object('id', 1, 'name', 'stash')")
                    :json-build-object))))

       (testing "selecting Arrays"
         (is (= [1 2]
                (-> (db/find-first db "select ARRAY[1,2]")
                    :array)))))

     (testing "selecting with honeysql map"
       (is (= 1 (-> (db/find-first db {:select [[1 :one]]})
                    :one)))

       (is (= (-> (db/find-first db {:select [[(sql/raw "'{\"a\": \"1\"}'::jsonb") :json]]})
                  :json)
              {:a "1"}))

       (testing "json access"
         (is (= (-> (db/find-first db {:select [[(sql/raw "'{\"a\": \"1\"}'::jsonb->'a'") :json]]})
                    :json)
                "1"))
         )))))
