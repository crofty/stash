(ns stash.test.html-test
  (:require [clojure.test :refer :all]
            [stash.test.html :as html]))

(deftest select-test
  (is (= 2 (count (html/select "<a>One</a><a>Two</a>" "a")))))

(deftest select-one-test
  (is (= "/foo" (-> (html/select-one "<a href=\"/foo\">One</a>" "a")
                    (.attr "href")))))

(deftest href-test
  (is (= "/foo" (html/href "<a href=\"/foo\">One</a>" "a"))))

(deftest exists-test
  (is (html/exists? "<a href=\"/foo\">One</a>" "a"))
  (is (not (html/exists? "<a href=\"/foo\">One</a>" "p"))))

(deftest text-test
  (is (= "One" (html/text "<a href=\"/foo\">One</a>" "a"))))

