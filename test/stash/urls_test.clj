(ns stash.urls-test
  (:require [clojure.test :refer :all]
            [stash.urls :as urls]
            [reitit.core :as r]))

(def router
  (r/router
   [["/api/ping" ::ping]
    ["/api/orders/:id" ::order]]))

(deftest path-for-test
  (is (= "/api/ping" (urls/path-for router ::ping)))
  (is (= "/api/orders/1" (urls/path-for router ::order {:id 1})))
  (is (= "/api/orders/1?a=b" (urls/path-for router ::order {:id 1} {:a "b"})))
  (is (thrown? Exception (urls/path-for router ::order)))
  (is (thrown? Exception (urls/path-for router ::order {:id nil}))))