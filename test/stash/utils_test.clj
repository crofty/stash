(ns stash.utils-test
  (:require [clojure.test :refer :all]
            [stash.utils :refer [get! get-in!] :as u]))

(deftest get-bang-test
  (is (= 1 (get! {:a 1} :a) ))
  (is (thrown? Exception (get! {:a 1} :b) )))

(deftest get-in-bang-test
  (is (= 1 (get-in! {:a 1} [:a]) ))
  (is (thrown? Exception (get-in! {:a 1} [:b]) )))

(deftest pluralize-test
  (is (= "1 dog"
         (u/pluralize 1 "dog")))
  (is (= "2 dogs"
         (u/pluralize 2 "dog")))
  (is (= "2 sheep"
         (u/pluralize 2 "sheep" "sheep"))))
