(ns stash.db.url-test
  (:require [clojure.test :refer :all]
            [stash.db.url :as db.url]))

(deftest ^:unit parse-database-url-test
  (testing "when user info is specified in authority component"
    (is (= {:adapter "postgresql"
            :database-name "stash"
            :host "postgres"
            :user "postgres"
            :password "insecure"}
           (db.url/parse "postgresql://postgres:insecure@postgres/stash"))))

  (testing "when user info is specified as query params"
    (is (= {:adapter "postgresql"
            :database-name "stash"
            :host "postgres"
            :user "postgres"
            :password "insecure"}
           (db.url/parse "postgresql://postgres/stash?user=postgres&password=insecure"))))

  (is (= {:adapter "postgresql"
          :database-name "stash"
          :host "postgres"
          :user "postgres"
          :password "insecure"}
         (db.url/parse "jdbc:postgresql://postgres/stash?user=postgres&password=insecure")))

  (is (= {:adapter "postgresql"
          :database-name "stash"
          :host "localhost"
          :user "postgres"
          :port 5460
          :password "insecure"}
         (db.url/parse "postgresql://postgres:insecure@localhost:5460/stash"))))

(deftest ^:unit ->jdbc-url
  (testing "when user info is specified in authority component"
    (is (= "jdbc:postgresql://postgres/stash_1234?user=postgres&password=insecure"
           (db.url/->jdbc-url {:adapter "postgresql"
                               :database-name "stash_1234"
                               :host "postgres"
                               :user "postgres"
                               :password "insecure"}))))

  (testing "with a port"
    (is (= "jdbc:postgresql://localhost:5460/stash?user=postgres&password=insecure"
           (db.url/->jdbc-url {:adapter "postgresql"
                               :database-name "stash"
                               :host "localhost"
                               :user "postgres"
                               :port 5460
                               :password "insecure"}))))
  (testing "when passed a psql url"
    (is (= "jdbc:postgresql://postgres:5432/farillio_1234?user=postgres&password=insecure"
           (db.url/->jdbc-url "postgresql://postgres:insecure@postgres:5432/farillio_1234")))
    (is (= "jdbc:postgresql://postgres:5432/farillio_1234?user=postgres&password=insecure"
           (db.url/->jdbc-url "postgres://postgres:insecure@postgres:5432/farillio_1234")))))

(deftest ^:unit ->psql-url
  (testing "when passed a map of options"
    (is (= "postgresql://postgres:insecure@postgres:5432/farillio_1234"
           (db.url/->psql-url {:adapter "postgresql"
                               :database-name "farillio_1234"
                               :host "postgres"
                               :port 5432
                               :user "postgres"
                               :password "insecure"}))))
  (testing "when passed a jdbc url"
    (is (= "postgresql://postgres:insecure@postgres:5432/farillio_1234"
           (db.url/->psql-url "jdbc:postgresql://postgres:5432/farillio_1234?user=postgres&password=insecure")))))
