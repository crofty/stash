(ns stash.json-test
  (:require [clojure.test :refer :all]
            [stash.json :as json]))

(deftest write-str-test
  (is (= "{}"
         (json/write-str {})))
  (is (= "{\"a\":1}"
         (json/write-str {:a 1}))))

(deftest read-json-test
  (is (= {}
         (json/read-json "{}")))
  (is (= {:a 1}
         (json/read-json "{\"a\":1}"))))