(ns stash.state-machines)

(defn next-state
  [fsm current-state transition]
  (get-in fsm [current-state transition]))