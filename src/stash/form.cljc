(ns stash.form
  (:require [clojure.string :as str]
            [medley.core :as m]
            #?(:clj [ring.util.codec :as codec])))

(defn field-name [keyword-or-string]
  (cond (string? keyword-or-string)
        keyword-or-string
        (keyword? keyword-or-string)
        (str/join "/"
                  (keep identity
                        [(namespace keyword-or-string)
                         (name keyword-or-string)]))))

#?(:clj
   (defn encode [attributes]
     (codec/form-encode (m/map-keys field-name attributes))))

(defn- remove-namespace [k]
  (keyword (name k)))

(defn- remove-namespaces-from-keys [m]
  (into {} (map (fn [[k v]] [(remove-namespace k) v]) m)))

(defn params
  "Select and normalizes form parameters from the request"
  ([request]
   (-> (or
        (:form-params request)
        (:multipart-params request))
       (remove-namespaces-from-keys)))
  ([request keys] (select-keys (params request) keys)))

