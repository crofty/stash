(ns stash.request)

(defn host
  [{:keys [server-name]}]
  server-name)

(defn port
  [request]
  (or (when-let [forwarded-port (get-in request [:headers "x-forwarded-port"])]
        (Integer/parseInt forwarded-port))
      (:port request)))

(defn scheme
  [request]
  (keyword (or (get-in request [:headers "x-forwarded-proto"])
               (:scheme request))))

(defn https?
  [request]
  (= :https (scheme request)))
