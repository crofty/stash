(ns stash.model
  (:require [medley.core :as m])
  (:refer-clojure :exclude [parse-boolean]))

(defmulti from-param
          (fn [k v] k))

(defmethod from-param :default [_ v] v)

(defn parse-boolean [v]
  #?(:clj (Boolean/parseBoolean (if (coll? v)
                                  (last v)
                                  v))))

#?(:clj (defn find-enums [v enum]
          (let [names (into #{} (flatten [v]))]
            (filter #(names (name %)) enum))))

#?(:clj (defn find-enum [v enum]
          (first (find-enums v enum))))

#?(:clj (defn coerce-param [name value]
          (from-param name value)))

#?(:clj (defn coerce-params [params]
          (m/map-kv (fn [k v] [k (coerce-param k v)]) params)))

