(ns stash.urls
  (:require #?(:cljs [reitit.frontend :as rf])
            [reitit.core :as r]))

(defn match->path
  ([match]
   (match->path match {}))
  ([match query-params]
   (r/match->path match query-params)))

(defn- match-by-name!
  [router route-name params]
  #?(:clj (r/match-by-name! router route-name params)
     :cljs (rf/match-by-name! router route-name params)))

(defn path-for
  ([router route-name]
   (path-for router route-name nil))
  ([router route-name params]
   (path-for router route-name params nil))
  ([router route-name params query]
   (or (-> (match-by-name! router route-name params)
           (match->path query))
       (throw (ex-info "path-for generated a nil path" {:route-name route-name :params params :query query})))))


