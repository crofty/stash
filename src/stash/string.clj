(ns stash.string
  (:import java.util.Base64))

(defn str->b64 [string]
  (.encodeToString (Base64/getEncoder) (.getBytes string)))

(defn b64->str [b64]
  (String. (.decode (Base64/getDecoder) b64)))
