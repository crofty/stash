(ns stash.db.url
  (:require [clojure.string :as str]
            [clojure.walk :as walk])
  (:import [java.net URI]))

(defn- split-param [param]
  (->
   (str/split param #"=")
   (concat (repeat ""))
   (->> (take 2))))

(defn- query->map
  [qstr]
  (when (not (str/blank? qstr))
    (some->> (str/split qstr #"&")
             seq
             (mapcat split-param)
             (apply hash-map)
             (walk/keywordize-keys))))

(defn parse [database-url]
  (let [url (URI. (str/replace-first database-url #"^jdbc:" ""))
        host (.getHost url)
        port (let [p (.getPort url)]
               (and (pos? p) p))
        path (.getPath url)
        scheme (.getScheme url)
        user-info (if-let [authority (.getUserInfo url)]
                    (let [[un pw] (str/split authority #":")]
                      (cond-> {:user un}
                        pw (assoc :password pw)))
                    (query->map (.getQuery url)))]
    (cond->
     {:adapter (get {"postgres" "postgresql"
                     "postgresql" "postgresql"} scheme scheme)
      :host host
      :database-name (str/replace path #"^/" "")}
      port (assoc :port port)
      user-info (merge user-info))))

(defn ->jdbc-url [db-spec]
  (if (string? db-spec)
    (-> db-spec parse ->jdbc-url)
    (let [{:keys [adapter host port database-name user password]} db-spec]
      (str "jdbc:" adapter "://" host
           (when port (str ":" port)) "/"
           database-name "?" "user=" user "&" "password=" password))))

(defn ->psql-url [db-spec]
  (if (string? db-spec)
    (-> db-spec parse ->psql-url)
    (let [{:keys [adapter host port database-name user password]} db-spec]
      (str adapter "://" user ":" password "@" host
           (when port (str ":" port)) "/" database-name))))

