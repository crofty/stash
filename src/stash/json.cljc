(ns stash.json
  #?(:clj (:require [jsonista.core :as json])
     :cljs (:require [cljs-bean.core :refer [->clj ->js]])))

(defn write-str [clj]
  #?(:clj (json/write-value-as-string clj)
     :cljs (.stringify js/JSON (->js clj))))

(defn read-json [clj]
  #?(:clj (json/read-value clj json/keyword-keys-object-mapper)
     :cljs (->clj (.parse js/JSON clj))))
