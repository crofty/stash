(ns stash.test.html
  (:import [org.jsoup Jsoup]))

(defn select [html-string selector]
  (-> (Jsoup/parse html-string)
      (.select selector)))

(defn exists? [html-string selector]
  (not (-> (Jsoup/parse html-string)
           (.select selector)
           (.isEmpty))))

(defn text [html-string selector]
  (-> (Jsoup/parse html-string)
      (.select selector)
      (.text)))

(defn select-one [html-string selector]
  (let [nodes (select html-string selector)]
    (when (< 1 (count nodes))
      (throw (ex-info "Multiple elements found, expected one" {:nodes nodes})))
    (first nodes)))

(defn href [html-string selector]
  (some-> (select-one html-string selector)
          (.attr "href")))

(defn value [html-string selector]
  (some-> (select-one html-string selector)
          (.attr "value")))

(defn src [html-string selector]
  (some-> (select-one html-string selector)
          (.attr "src")))

(defn title [html-string]
  (text html-string "head>title"))

