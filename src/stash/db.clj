(ns stash.db
  (:require [next.jdbc :as jdbc]
            [next.jdbc.date-time]
            [next.jdbc.result-set :as rs]
            [next.jdbc.prepare :as prepare]
            [honeysql.core :as sql]
            [next.jdbc.connection :as connection]
            [jsonista.core :as json]
            [honeysql.format :as fmt]
            [clojure.string :as str])
  (:import (com.zaxxer.hikari HikariDataSource)
           (org.postgresql.util PGobject)
           [java.sql PreparedStatement]
           [java.sql Array]))

(set! *warn-on-reflection* true)

;; https://github.com/seancorfield/next-jdbc/blob/c76d2241a8d6f26bc86975a91cb7c6615f1118a6/doc/tips-and-tricks.md
(def mapper (json/object-mapper {:decode-key-fn keyword}))
(def ->json json/write-value-as-string)
(def <-json #(json/read-value % mapper))

(extend-protocol rs/ReadableColumn
  Array
  (read-column-by-label [^Array v _]    (vec (.getArray v)))
  (read-column-by-index [^Array v _ _]  (vec (.getArray v))))

(defn ->pgobject
  "Transforms Clojure data to a PGobject that contains the data as
  JSON. PGObject type defaults to `jsonb` but can be changed via
  metadata key `:pgtype`"
  [x]
  (let [pgtype (or (:pgtype (meta x)) "jsonb")]
    (doto (PGobject.)
      (.setType pgtype)
      (.setValue (->json x)))))

(defn <-pgobject
  "Transform PGobject containing `json` or `jsonb` value to Clojure
  data."
  [^org.postgresql.util.PGobject v]
  (let [type  (.getType v)
        value (.getValue v)]
    (if (#{"jsonb" "json"} type)
      (when value
        (let [json-val (<-json value)]
          (if (instance? clojure.lang.IObj json-val)
            (with-meta json-val {:pgtype type})
            json-val)))
      value)))

;; if a SQL parameter is a Clojure hash map or vector, it'll be transformed
;; to a PGobject for JSON/JSONB:
(extend-protocol prepare/SettableParameter
  clojure.lang.IPersistentMap
  (set-parameter [m ^PreparedStatement s i]
    (.setObject s i (->pgobject m)))

  clojure.lang.IPersistentVector
  (set-parameter [v ^PreparedStatement s i]
    (.setObject s i (->pgobject v))))

;; if a row contains a PGobject then we'll convert them to Clojure data
;; while reading (if column is either "json" or "jsonb" type):
(extend-protocol rs/ReadableColumn
  org.postgresql.util.PGobject
  (read-column-by-label [^org.postgresql.util.PGobject v _]
    (<-pgobject v))
  (read-column-by-index [^org.postgresql.util.PGobject v _2 _3]
    (<-pgobject v)))

(defn from-url [jdbc-url]
  (connection/->pool HikariDataSource {:jdbcUrl jdbc-url}))

(defn format-query
  [query]
  (cond
    (map? query) (sql/format query)
    (string? query) [query]
    :else query))

(defn execute!
  [database query]
  (jdbc/execute! database (format-query query) jdbc/unqualified-snake-kebab-opts))

(defn find-many
  [database query]
  (jdbc/execute! database (format-query query) jdbc/unqualified-snake-kebab-opts))

(defn find-first
  [database query]
  (jdbc/execute-one! database (format-query query) jdbc/unqualified-snake-kebab-opts))

(defn sql-cast [value type]
  (sql/call :cast value type))

(defn cast-json [value]
  (sql-cast value :json))

(defn cast-jsonb [value]
  (sql-cast value :jsonb))

(defn cast-bigint [value]
  (sql-cast value :bigint))

(defn cast-integer [value]
  (sql-cast value :integer))

(defn cast-text [value]
  (sql-cast value :text))

(defn cast-timestamp [value]
  (sql-cast value :timestamp))

(defn cast-date [value]
  (sql-cast value :date))

(defn cast-boolean [value]
  (sql-cast value :boolean))

(def current-timestamp (sql/raw "CURRENT_TIMESTAMP"))
(def current-date (sql/raw "CURRENT_DATE"))
(def statement-timestamp (sql/call :statement_timestamp))

(defmethod fmt/fn-handler "match" [op & args]
  (fmt/paren-wrap (str/join " @@ " (map fmt/to-sql-value args))))


(defn by-id
  "Returns the first row from the dataset that matches the id.
   A dataset can either be a table name or a select statement"
  ([database dataset id]
   (by-id database dataset id [:*]))
  ([database dataset id columns]
   (find-first database {:select columns
                         :from [[dataset :_t0]]
                         :where [:= :_t0.id id]})))

(defn update-by-id!
  "Updates the rows in the table that match the id."
  ([database table id values]
   (execute! database {:update table
                       :set values
                       :where [:= :id id]})))

(defn row-count
  ([database dataset]
   (-> database
       (find-first {:select [:%count._t0]
                    :from [[dataset :_t0]]})
       :count))
  ([database dataset condition]
   (row-count database {:select [:*]
                        :from [[dataset :_t1]]
                        :where condition})))

(defn exists?
  [database query]
  (-> (find-first database {:select [[true :bool]] :where [:exists query]})
      :bool))

(defn truncate! [database table]
  (execute! database {:delete-from table}))

(defn responsive?
  [database]
  (boolean
   (try
     (jdbc/execute-one! database ["SELECT 1"] {:timeout 1})
     (catch Exception e false))))