(ns stash.interceptors
  (:require [stash.model :as model]
            [clojure.walk :as walk]))

(def coerce-form-values
  {:name :coerce-form-values
   :enter (fn [ctx]
            (if-let [form-params (get-in ctx [:request :form-params])]
              (assoc-in ctx [:request :form-params] (model/coerce-params form-params))
              (if-let [multipart-params (get-in ctx [:request :multipart-params])]
                (assoc-in ctx [:request :multipart-params]
                          (model/coerce-params (walk/keywordize-keys multipart-params)))
                ctx)))})
