(ns stash.transit
  #?(:clj (:refer-clojure :exclude [read]))
  (:require [time-literals.read-write]
            [cognitect.transit :as transit]
            #?(:cljs [java.time :refer [Period
                                        LocalDate
                                        LocalDateTime
                                        ZonedDateTime
                                        OffsetTime
                                        Instant
                                        OffsetDateTime
                                        ZoneId
                                        DayOfWeek
                                        LocalTime
                                        Month
                                        Duration
                                        Year
                                        YearMonth]]))
  #?(:clj (:import (java.io ByteArrayOutputStream ByteArrayInputStream)
                   (java.time Period
                              LocalDate
                              LocalDateTime
                              ZonedDateTime
                              OffsetTime
                              Instant
                              OffsetDateTime
                              ZoneId
                              DayOfWeek
                              LocalTime
                              Month
                              Duration
                              Year
                              YearMonth))))

;; A lot of this is from https://gist.github.com/jjttjj/6bc0b62ef1dbf29c1c69ea22f8eb7f55
;; extended to allow additional read and write handlers to be added

(def time-classes
  {'period Period
   'date LocalDate
   'date-time LocalDateTime
   'zoned-date-time ZonedDateTime
   'instant Instant
   ;;'offset-time OffsetTime
   ;;'offset-date-time OffsetDateTime
   'time LocalTime
   'duration Duration
   'year Year
   'year-month YearMonth
   'zone ZoneId
   'day-of-week DayOfWeek
   'month Month})

(def write-handlers
  (into {}
    (for [[tick-class host-class] time-classes]
      [host-class (transit/write-handler (constantly (name tick-class)) str)])))

(def read-handlers
  (into {} (for [[sym fun] time-literals.read-write/tags]
             [(name sym) (transit/read-handler fun)]))) ; omit "time/" for brevity

(defn write-str "Encode data structure to transit."
  ([arg]
   (write-str arg {}))
  ([arg {:keys [handlers]
         :or {handlers {}}}]
   (let [opts {:handlers (merge write-handlers handlers)}]
     #?(:clj  (let [out (ByteArrayOutputStream.)
                    writer (transit/writer out :json opts)]
                (transit/write writer arg)
                (.toString out))
        :cljs (transit/write (transit/writer :json opts) arg)))))

(defn read "Decode data structure from transit."
  ([json]
   (read json {}))
  ([json {:keys [handlers]
          :or {handlers {}}}]
   (let [opts {:handlers (merge read-handlers handlers)}]
     #?(:clj  (try (let [in (ByteArrayInputStream. (.getBytes json))
                         reader (transit/reader in :json opts)]
                     (transit/read reader))
                   (catch Exception e
                     ;;(log/warn "Invalid message" json (:cause (Throwable->map e)))
                     :invalid-message))
        :cljs (transit/read (transit/reader :json opts) json)))))
