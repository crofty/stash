(ns stash.time
  (:require [tick.core :as t]
            [stash.utils :as u]))

(defn time-ago
  "Gives the time since an event in the most appropriate units of time"
  [event]
  (when event
    (let [duration (t/duration
                     {:tick/beginning event
                      :tick/end (t/now)})
          years (long (t/divide duration (t/new-duration 365 :days)))
          months (long (t/divide duration (t/new-duration (int (/ 365 12)) :days)))
          weeks (long (t/divide duration (t/new-duration 7 :days)))]
      (cond
        (> (t/days duration) 365)
        (str (u/pluralize years "year") " ago")

        (and (<= (t/days duration) 365) (> (t/days duration) (/ 365 12)))
        (str (u/pluralize months "month") " ago")

        (and (<= (t/days duration) (/ 365 12)) (> (t/days duration) 7))
        (str (u/pluralize weeks "week") " ago")

        (and (<= (t/days duration) 7) (> (t/days duration) 1))
        (str (u/pluralize (t/days duration) "day") " ago")

        (and (<= (t/days duration) 1) (> (t/hours duration) 1))
        (str (u/pluralize (t/hours duration) "hour") " ago")

        (and (<= (t/hours duration) 1) (> (t/minutes duration) 1))
        (str (u/pluralize (t/minutes duration) "minute") " ago")

        (and (<= (t/minutes duration) 1) (> (t/seconds duration) 1))
        (str (u/pluralize (t/seconds duration) "second") " ago")

        (t/< (t/now) event)
        ""))))
