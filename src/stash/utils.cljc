(ns stash.utils
  (:require #?(:cljs [com.cognitect.transit.types :as ty])
            [clojure.string :as string]))

(defprotocol IntoInt
  (->int [v] "Converts the given value into an int so that (int? result) => true"))

#?(:clj
   (extend-protocol IntoInt
     java.lang.Long
     (->int [v] v)
     java.lang.Integer
     (->int [v] v)
     java.lang.String
     (->int [v] (Long/parseLong v))
     clojure.lang.Keyword
     (->int [v] (->int (name v))))
   :cljs
   (extend-protocol IntoInt
                    number
                    (->int [v] v)
                    string
                    (->int [v]
                           (ty/intValue v))))

(defn ->int?
  "Coerces the given value to an int (int? result) => true, or nil if coercion isn't possible"
  [v]
  (when (if (string? v)
          (re-matches #"\d+" v)
          (satisfies? IntoInt v))
    (->int v)))

(defn get!
  "A version of get that raises an error if the key is not found. Useful for pulling info
   out of spec-data as it highlights when an expected key is missing. Optionally takes a
   third message argument, included in the ex-info when the key is not found."
  ([m k] (get! m k (str "key not found " k)))
  ([m k msg]
   (let [result (get m k ::not-found)]
     (if (= result ::not-found)
       (throw (ex-info msg {:key k :map m}))
       result))))

(defn get-in!
  "A version of get-in that raises an error if the keys are not found. Useful for pulling info
   out of spec-data as it highlights when an expected key is missing. Optionally takes a
   third message argument, included in the ex-info when the keys are not found."
  ([m ks] (get-in! m ks (str "keys not found " ks)))
  ([m ks msg]
   (let [result (get-in m ks ::not-found)]
     (if (= result ::not-found)
       (throw (ex-info msg {:keys ks :map m}))
       result))))

(defn pluralize-without-count
  [count singular & [plural]]
  (if (= 1 count) singular (or plural (str singular "s"))))

(defn pluralize
  "Attempts to pluralize the word unless count is 1. If plural is
  supplied, it will use that when count is > 1, otherwise it will
  add an s."
  [count singular & [plural]]
  (str count " " (pluralize-without-count count singular plural)))