.PHONY: test dev

test:
	clojure -M:test

dev:
	docker-compose up
